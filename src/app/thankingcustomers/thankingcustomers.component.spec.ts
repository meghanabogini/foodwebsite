import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThankingcustomersComponent } from './thankingcustomers.component';

describe('ThankingcustomersComponent', () => {
  let component: ThankingcustomersComponent;
  let fixture: ComponentFixture<ThankingcustomersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ThankingcustomersComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThankingcustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
