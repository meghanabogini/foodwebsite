import { Component } from '@angular/core';
import { PopupService } from '../popup.service';

@Component({
  selector: 'app-thankingcustomers',
  templateUrl: './thankingcustomers.component.html',
  styleUrls: ['./thankingcustomers.component.css']
})
export class ThankingcustomersComponent {
  constructor(private popupService: PopupService) {}

  showSpecialOffer() {
    this.popupService.showPopup('Better luck next class');
  }
}

