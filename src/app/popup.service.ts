import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  private popupSubject = new Subject<any>();
  popupState$ = this.popupSubject.asObservable();

  constructor() { }

  showPopup(message: string) {
    this.popupSubject.next({ message });
  }

  closePopup() {
    this.popupSubject.next(null);
  }
}

