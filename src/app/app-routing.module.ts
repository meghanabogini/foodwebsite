import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BannerComponent } from './banner/banner.component';

import { ExploremenuComponent } from './exploremenu/exploremenu.component';
import { HealthyfoodComponent } from './healthyfood/healthyfood.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ThankingcustomersComponent} from './thankingcustomers/thankingcustomers.component';
import { WhychooseusComponent } from './whychooseus/whychooseus.component';

const routes: Routes = [
{path : 'banner' , component :BannerComponent},
{path : 'exploremenu' , component:ExploremenuComponent},
{path : 'healthyfood',component :HealthyfoodComponent},
{path : 'navbar',component :NavbarComponent},
{path : 'thankingcustomers', component : ThankingcustomersComponent},
{path : 'whychooseus',component :WhychooseusComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
