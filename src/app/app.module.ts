import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BannerComponent } from './banner/banner.component';
import { WhychooseusComponent } from './whychooseus/whychooseus.component';
import { ExploremenuComponent } from './exploremenu/exploremenu.component';
import { HealthyfoodComponent } from './healthyfood/healthyfood.component';

import { ThankingcustomersComponent } from './thankingcustomers/thankingcustomers.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PopupService } from './popup.service';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BannerComponent,
    ThankingcustomersComponent,
    WhychooseusComponent,
    ExploremenuComponent,
    HealthyfoodComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule
    
  ],
  providers: [PopupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
