import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthyfoodComponent } from './healthyfood.component';

describe('HealthyfoodComponent', () => {
  let component: HealthyfoodComponent;
  let fixture: ComponentFixture<HealthyfoodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HealthyfoodComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HealthyfoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
